#!/bin/bash

GAME_CHANNEL="prod"

for i in "$@"
do
  if [ "$i" = "--snapshot" ]
  then
    GAME_CHANNEL="snapshot"
  fi
done

echo "Channel: "$GAME_CHANNEL

GAME_DIR=$HOME/.config/PVPGame2/game/$GAME_CHANNEL
CURRENT_DIR=$(pwd)

echo "Game dir: "$GAME_DIR

if [[ -d $GAME_DIR ]] ; then
	echo "Game already downloaded, checking for updates..."
	cd $GAME_DIR
	git pull
	echo "Finished checking for updates"
else
	echo "Game not yet downloaded, downloading..."
	mkdir -p $GAME_DIR
	cd $GAME_DIR
	git clone http://a:@gitlab.cablepost.co.uk/builds/pvpgame2-$GAME_CHANNEL-linux64.git .
	echo "Downloaded"
fi

chmod 777 $GAME_DIR"/PVPGame2"

echo "-------- Starting Game --------"
$GAME_DIR"/PVPGame2"
echo "Game Closed"

cd $CURRENT_DIR