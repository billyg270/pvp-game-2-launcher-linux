# PVP Game 2 Launcher - Linux
This is a script that downloads / updates PVPGame and runs it.

## Requirements
To run this script you will need git installed on your system. To do this on Debian based systems, use `sudo apt install git`.

## Usage
Download the script `PVPGame2.sh` to a location of your choice, then run the script. You may need to mark the script as executable, this can be done through the terminal using `chmod 777 PVPGame2.sh`.

To play the snapshot version of the game, use `./PVPGame2.sh --snapshot`.

## Notes
The game will be downloaded to `/home/(your username)/.config/PVPGame2/game`. You can change this by modifying the script to your liking.